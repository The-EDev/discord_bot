import discord
import time
import io

welcome_channel_id = 0
training_channel_id = 0
rules_channel_id = 0
token = ""
ranks = {}

def get_timestamp():
	return int(time.time())

def calculate_points(start, end):
	return (end-start)

def get_rank(member):
	for role in member.roles:
		if (role.name in list(ranks.keys())):
		  return role.name
	return None

intents = discord.Intents.default()
intents.members = True

client = discord.Client(intents=intents)

@client.event
async def on_ready():
    print('Logged in as {0.user}'.format(client))

        
@client.event
async def on_member_join(member):
	welcome_channel = client.get_channel(welcome_channel_id)
	training_channel = client.get_channel(training_channel_id)
	rules_channel = client.get_channel(rules_channel_id)
	await welcome_channel.send("Welcome " + member.mention + ". Thank you for joining! Feel free to make use of " + (training_channel.mention) + " and voice channels. And don't forget to read the " + (rules_channel.mention) + '.')

#TODO fix duplicate entries in in_voice.hcat
@client.event
async def on_voice_state_update(member, before, after):

#User joined the VC
	if (not before.channel and after.channel and not after.afk):
		with open("in_voice.hcat", "a") as in_voice:
			in_voice.write(str(member.id) + ":-:" + str(get_timestamp()) + '\n')

#User either left VC or went AFK
	else:
		member_points = 0
		done_already = False

		with open ("in_voice.hcat", "r") as in_voice:
			lines = in_voice.readlines()

		with open ("in_voice.hcat", "w") as in_voice:
			for line in lines:
				if (line != '\n'):
					linesplit = line.strip("\n").rsplit(":-:")

					if (int(linesplit[0]) == member.id):
						member_points = calculate_points(int(linesplit[1]), get_timestamp())

					else:
						in_voice.write(line)

			in_voice.truncate()


		with open ("members.hcat", "r") as members:
			lines = members.readlines()

		with open ("members.hcat", "w") as members:
			for line in lines:
				if (line != '\n'):
					linesplit = line.strip("\n").rsplit(":-:")

					if (int(linesplit[0]) == member.id):
						old_points = int(linesplit[1])
						member_points += old_points
						members.write(str(member.id) + ":-:" + str(member_points) + '\n')
						done_already = True
					else:
						members.write(line)

			if (not done_already):
				members.seek(0, io.SEEK_END)
				members.write(str(member.id) + ":-:" + str(member_points) + '\n')

			members.truncate()
		with open ("members.hcat", "r") as members:
			lines = members.readlines()
		with open ("members.hcat", "w") as members:	
			member_rank = get_rank(member)
			new_rank = None
			done_already = False
			for rank, r_points in ranks.items():
				if (member_points >= r_points):
					new_rank = rank
			if (new_rank is not None):
				if (member_rank is not None):
					if (ranks[new_rank] > ranks[member_rank]):
						old_role = discord.utils.get(member.guild.roles, name=member_rank)
						new_role = discord.utils.get(member.guild.roles, name=new_rank)
						await member.remove_roles(old_role)
						await member.add_roles(new_role)
					elif (ranks[new_rank] < ranks[member_rank]):
						members.seek(0)
						actual_points = ranks[member_rank] + member_points
						for line in lines:
							if (line != '\n'):
								linesplit = line.strip("\n").rsplit(":-:")

								if (int(linesplit[0]) == member.id):
									members.write(str(member.id) + ":-:" + str(actual_points) + '\n')
								else:
									members.write(line)

						members.truncate()
				else:
					new_role = discord.utils.get(member.guild.roles, name=new_rank)
					await member.add_roles(new_role)
			elif (member_rank is not None):
				members.seek(0)
				actual_points = ranks[member_rank] + member_points
				for line in lines:
					if (line != '\n'):
						linesplit = line.strip("\n").rsplit(":-:")

						if (int(linesplit[0]) == member.id):
							members.write(str(member.id) + ":-:" + str(actual_points) + '\n')
						else:
							members.write(line)
				members.truncate()
			else:
				for line in lines:
					members.write(line)

client.run(token)
